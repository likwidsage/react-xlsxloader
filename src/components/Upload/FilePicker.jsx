import React, { useContext, useEffect } from 'react'
import { FilesContext } from '../../App'
import { useDropzone } from 'react-dropzone'
import {withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import PropTypes from 'prop-types'

const styles = theme => ({
  dropZone: {
    margin: 0,
    padding: 8,
    marginTop: 64,
    border: theme.palette.primary.light,
    color: theme.palette.primary.contrastText,
  },
  paper: {
    margin: 8,
  },
})

const FilePicker = (props) => {
  const {fileSet, setFileSet} = useContext(FilesContext)
  const {classes} = props

  useEffect(() => {
    console.log('Picker', fileSet)
  }, [fileSet])

  const onDrop = dropFiles => {
    // Filter XLSX files
    const validFiles = (dropFiles.filter(file => {
      if(file.name.endsWith('.xlsx') || file.name.endsWith('.xls')){
        return true
      }
      return false
    }))

    const newData = validFiles.map(file => {
      let tableName = file.name.substring(0,25)
      tableName = tableName.split(' ').join('_')
        .toLowerCase()
        .replace('.xlsx', '')
        .replace('.xls', '')      
      return {file, tableName, hasHeader: true, dbType: 'oracle'}
    })
    const tFiles = [...fileSet]
    const newFileSet = [...tFiles, ...newData]
    
    setFileSet(newFileSet)
  }

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

  return (
    <Paper className={classes.paper}>
      <div {...getRootProps()} className={classes.dropZone}>
        <input {...getInputProps()} />
        {!isDragActive ?
          <p>Drop here to upload. (click to select files)</p> :
          <p>Let it go. Let it go. (click to select files)</p>}
      </div>
    </Paper>
  )
}

FilePicker.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(FilePicker)
