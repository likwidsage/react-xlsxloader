const express = require('express')
const router = express.Router()
const multer = require('multer')
const mongoose = require('mongoose')
const Settings = require('../src/config/Settings')
const taskSchema = require('../schemas/taskSchema')
const Task = mongoose.model('task', taskSchema)

mongoose.connect(Settings.MongoDb, {
  useNewUrlParser: true,
})
let db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, Settings.uploadDir)
  },

  filename: (req, file, cb) => {
    let fileName = file.originalname.split('.')
    fileName.push(fileName[fileName.length - 1])
    fileName[fileName.length - 2] = `.${Date.now()}.`
    fileName = fileName.join('')
    cb(null, fileName)
  },
})

const upload = multer({ storage })
router.post('/upload', upload.array('files'), (req, res) => {
  const files  = req.files 
  console.log('upload', files)

  const filePaths = files.map(f => f.path)

  return res.status(200).json(filePaths)
})

router.post('/add-task', (req, res) => {
  const tasks = req.body
  console.log('add-task', tasks) 

  Task.insertMany(tasks)
    .then(docs => {
      console.log('saved', docs)
      return res.status(200).json(docs)
    })
    .catch(err => {
      console.warn('save-err', err)
      return res.status(400).json(err)
    })
})

module.exports = router
