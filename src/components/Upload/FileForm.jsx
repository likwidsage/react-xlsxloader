import React, { useContext, useState } from 'react'
import { FilesContext } from '../../App'
import axios from 'axios'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import Delete from '@material-ui/icons/Delete'
import Typography from '@material-ui/core/Typography'
import { Select, MenuItem, Checkbox, FormControlLabel } from '@material-ui/core'

const styles = theme => ({
  main: {
    margin: 24,
  },
  item: {
    backgroundColor: '#eee',
    padding: 8,
  },
  subitem: {
    backgroundColor: '#ccc',
    marginTop: 16,
    padding: 8,
    borderRadius: 4,
  },
  submitButton: {
    color: theme.palette.error.contrastText,
  },
  deleteIcon: {
    color: '#cc0000',
  },
})

const FileForm = props => {
  const { fileSet, setFileSet } = useContext(FilesContext)
  const [redirect, SetRedirect] = useState(false)
  const { classes } = props

  const HandleSubmit = () => {
    const error = {}
    // Check for errors
    fileSet.forEach(set => {
      if (!set.tableName && !error.missingTableName) {
        error.missingTableName = true
      } else if (set.tableName && !error.invalidName) {
        if (set.tableName.length < 10 || set.tableName.length > 30) {
          error.invalidName = true
        } else if (!set.tableName.startsWith('XLSX_')) {
          error.invalidName = true
        }
      }
    })

    let formData = new FormData()
    fileSet.forEach(set => {
      formData.append('files', set.file)
      formData.append('tableNames', set.tableName)
      formData.append('dbType', set.dbType)
      formData.append('hasHeader', set.hasHeader)
    })

    // Post files
    axios.post('/api/file/upload', formData)
      .then(res => {
        const filePaths = res.data
        console.log('filePaths',filePaths)
        
        const tasks = fileSet.map((set, i) => {
          return {
            filePath: filePaths[i],
            tableName: set.tableName,
            dbType: set.dbType,
            hasHeader: set.hasHeader,
          }
        })

        console.log('fileform', tasks)

        axios.post('/api/file/add-task', tasks)
          .then(fileSet => {
            console.log(fileSet)
            SetRedirect(true)
          })
          .catch(err => {
            console.error(err)
          })
      })
      .catch(err => {
        console.error(err)
      })
  }

  const HandleChange = (e, i, pName) => {
    console.log(i, e.target.value || e.target.checked)
    const tFiles = [...fileSet]
    const tFile = tFiles[i]
    tFiles.splice(i, 1, {...tFile, [pName]: e.target.value || e.target.checked })
    console.log(tFiles)
    setFileSet(tFiles)
  }

  const handleDelete = i => {
    const tFiles = [...fileSet]
    tFiles.splice(i, 1)
    console.log('Delete:', i, tFiles)
    setFileSet(tFiles)
  }

  return (
    <div className={classes.main}>
      {fileSet.length > 0 ? (
        <Grid container spacing={1}>
          {fileSet.map((data, i) => (
            <Grid item key={i} xs={4}>
              <Paper className={classes.item}>
                
                <Grid container direction="row" alignItems="center">
                  <Grid item xs={2} align="left">
                    {i+1}
                  </Grid>
                  <Grid item xs={8}>
                    <Typography variant="subtitle2" align="center">
                      {fileSet[i].file.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={2} align="right">
                    <Delete
                      onClick={() => handleDelete(i)}
                      className={classes.deleteIcon}
                    />
                  </Grid>
                </Grid>

                <Grid container className={classes.subitem}>
                  <Grid item xs={12}>
                    <TextField
                      key={i}
                      fullWidth
                      defaultValue={fileSet[i].tableName}
                      onBlur={e => {
                        HandleChange(e, i, 'tableName')
                      }}
                      inputProps={{ 
                        maxLength: '25',
                        startadornment: (
                          <InputAdornment position="start">XLSX_</InputAdornment>
                        ),
                      }}
                      label="Table Name"
                    />
                  </Grid>

                  <Grid item xs={6}>
                    <Select 
                      onChange={e => {HandleChange(e, i, 'dbType')}}
                      value={fileSet[i].dbType}
                    >
                      <MenuItem value='oracle'>Oracle</MenuItem>
                      <MenuItem value='mssql'>MS SQL</MenuItem>
                    </Select>
                  </Grid>

                  <Grid item xs={6}>
                    <FormControlLabel
                      control={
                        <Checkbox 
                          checked={fileSet[i].hasHeader} 
                          onChange={e => HandleChange(e, i, 'hasHeader')}
                        />
                      } 
                      label="Header"
                    />
                  </Grid>
                </Grid>

              </Paper>
            </Grid>
          ))}

          <Grid item xs={12} align="right">
            <Button
              color="primary"
              variant="contained"
              className={classes.submitButton}
              onClick={HandleSubmit}
            >
              Import
            </Button>
          </Grid>
        </Grid>
      ) : null}
      {redirect 
        ? <Redirect push to="/monitor" />
        : null
      }
    </div>
  )
}

FileForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(FileForm)
