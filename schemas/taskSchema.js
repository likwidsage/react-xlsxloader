const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema(
  {
    filePath: {
      type: String,
      required: true,
    },
    tableName: {
      type: String,
      required: true,
    },
    hasHeader:{
      type: Boolean,
      required: true,
      default: true,
    },
    dbType: {
      type: String,
      enum: ['oracle', 'mssql'],
      required: true,
    },
    running: {
      type: Boolean,
      default: false,
    },
    processed: {
      type: Boolean,
      default: false,
    },
    error: {
      type: String,
      default: null,
    },
    createdDate: {
      type: Date,
      default: new Date(),
    },
  }
)

module.exports = taskSchema