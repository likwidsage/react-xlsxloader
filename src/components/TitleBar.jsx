import React from 'react'
import PropTypes from 'prop-types'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Grid from '@material-ui/core/Grid'

import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'

import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'


const styles = () => ({
  titleBar: {
    padding: 4,
  },
})

const TitleBar = (props) => {
  const { classes } = props
  const { location } = props

  return (
    <AppBar className={classes.titleBar}>
      <Grid container>
        <Grid item xs>
          <Typography variant='h4' color='textPrimary'>
            XLSX Loader
          </Typography>
        </Grid>
        <Grid item xs>
          <Tabs value={location.pathname} >
            <Tab value="/" label="Upload" component={Link} to='/' />
            <Tab value="/monitor" label="Monitor" component={Link} to='/monitor' />
            <Tab 
              value={location.pathname.match(/\/view\/.*/) ? location.pathname : null} 
              label="View"
              disabled={!location.pathname.match(/\/view\/.*/)} 
            />
          </Tabs>
        </Grid>
      </Grid>
    </AppBar>
  )
}

TitleBar.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

export default withRouter(withStyles(styles, {withTheme: true})(TitleBar))