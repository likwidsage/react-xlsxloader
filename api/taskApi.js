const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Settings = require('../src/config/Settings')
const taskSchema = require('../schemas/taskSchema')
const Task = mongoose.model('task', taskSchema)

mongoose.connect(Settings.MongoDb, {
  useNewUrlParser: true,
  useFindAndModify: false,
})
let db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

router.get('/', (req, res) => {
  Task.find()
    .then(tasks => {
      return res.status(200).json(tasks)
    })
})

router.get('/:id', (req, res) => {
  const {id} = req.params

  Task.find({_id: id})
    .then(task => {
      return res.status(200).json(task[0])
    })
})

router.post('/run/', (req, res)=>{
  const {id} = req.body
  console.log(id)
  Task.updateOne({_id: id}, {running: true})
    .then(() => {
      Task.find().then(records => {
        res.status(202).location(`/api/queue/${id}`).json(records)
      })
    })
})

router.post('/cancel/', (req, res)=>{
  const {id} = req.body
  console.log(id)
  Task.find({_id: id}).then(t => {
    if(!t.proccesed){
      Task.updateOne({_id: id}, {running: false})
        .then(() => {
          Task.find().then(records => {
            res.status(202).json(records)
          })
        })
    }
  })
})

module.exports = router