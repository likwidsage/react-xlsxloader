import React, { useState, useEffect } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { Link } from 'react-router-dom'
import Settings from '../config/Settings'
import socketIoClient from 'socket.io-client'
const socket = socketIoClient(`${Settings.BaseUrl}:${Settings.ApiPort}`)


const styles = theme => ({
  root: {
    marginTop: 64,
    marginLeft: 8,
    marginRight: 8,
    flexGrow: 1,
  },
})

const monitor = (props) => {
  const [tasks, setTasks] = useState([])
  const {classes} = props

  const refresh = () => {
    return new Promise((resolve, reject) => {
      axios.get('/api/tasks/')
        .then(res => {
          console.log('tasks: ', res.data)
          setTasks(res.data)
          resolve(res.data)
        })
        .catch(err => {
          reject(err)
        })
    })}

  useEffect(() => {
    refresh()
  },[])

  useEffect(() => {
    socket.on('updateTask', updatedTask => {
      const newTasks = tasks.map(task => {
        if (task._id === updatedTask._id){
          return updatedTask
        }
        return task
      })
      setTasks(newTasks)
    })

    return () => {
      socket.off('updateTask')
    }
  })

  return (
    <Table className={classes.root}>
      <TableHead>
        <TableRow>
          <TableCell>
              Path
          </TableCell>
          <TableCell>
              Destination Table
          </TableCell>
          <TableCell>
              Status
          </TableCell>
          <TableCell>
              View
          </TableCell>
        </TableRow>
      </TableHead>
      {tasks.length > 0 ? (
        <TableBody>
          {tasks.map((task, i) => {
            return (
              <TableRow key={i}>
                <TableCell>
                  {task.filePath}
                </TableCell>
                <TableCell>
                  {task.dbType} - {task.tableName}
                </TableCell>
                <TableCell>
                  {task.running 
                    ? 'In Progress'
                    : task.error 
                      ? 'Error ❌'
                      : task.processed
                        ? 'Done ✔️'
                        : 'Waiting' }
                </TableCell>
                <TableCell>
                  <Link to={`/view/${task._id}`}>
                      View
                  </Link>
                </TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      ):(
        <TableBody>
          <TableRow>
            <TableCell>
              No Data
            </TableCell>
          </TableRow>
        </TableBody>
      )}
    </Table>
  )
}

monitor.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(monitor)