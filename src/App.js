import React, { useState, useEffect } from 'react'
import { BrowserRouter, Route} from 'react-router-dom'

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'
import lightBlue from '@material-ui/core/colors/lightBlue'

import TitleBar from './components/TitleBar'
import FilePicker from './components/Upload/FilePicker'
import FileForm from './components/Upload/FileForm'
import Monitor from './components/Monitor'
import View from './components/View'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: green,
    secondary: lightBlue,
  },
})

export const FilesContext = React.createContext({fileSet: [], setFileList: () => {}})

const App = () => {
  const [fileSet, setFileSet] = useState([])

  useEffect(() => {
    console.log('fileset: ',fileSet)
  }, [fileSet])

  const uploadSite = () => {
    return (
      <FilesContext.Provider value={{fileSet, setFileSet}}>
        <FilePicker/>
        <FileForm/>
      </FilesContext.Provider>
    )
  }

  return (
    <MuiThemeProvider theme={theme}>
      <BrowserRouter>
        <TitleBar />
        <Route path='/' exact render={uploadSite} />
        <Route path='/monitor' exact render={() => <Monitor />} />  
        <Route path='/view/:id' component={View} />  
      </BrowserRouter>
    </MuiThemeProvider>
  )
}

export default App
