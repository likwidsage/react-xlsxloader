const express = require('express')
const router = express.Router()
const Settings = require('../src/config/Settings')
const mongoose = require('mongoose')
const taskSchema = require('../schemas/taskSchema')
const Task = mongoose.model('task', taskSchema)
const fs = require('fs')

mongoose.connect(Settings.MongoDb, {
  useNewUrlParser: true,
  useFindAndModify: false,
})
let db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

router.post('/started', (req, res) => {
  const {id} = req.body
  const io = req.app.get('socketio')

  Task
    .findByIdAndUpdate({ _id: id }, { processed: false, running: true }, {new: true})
    .then(updatedTask => {
      console.log('Emit running', updatedTask)
      io.emit('updateTask', updatedTask)
      return res.status(200).json(updatedTask)
    })
})

router.post('/completed', (req, res) => {
  const {id} = req.body
  const io = req.app.get('socketio')

  Task
    .findByIdAndUpdate({ _id: id }, { processed: true, running: false },  {new: true})
    .then(updatedTask => {
      fs.unlink(updatedTask.filePath, err => {
        if (err){
          return console.error(err)
        }

        console.log('Emit completed', updatedTask)
        io.emit('updateTask', updatedTask)
        return res.status(200).json(updatedTask)
      })
    })
})

router.post('/error', (req, res) => {
  const {id, error} = req.body
  const io = req.app.get('socketio')

  Task
    .findByIdAndUpdate({ _id: id }, { processed: false, running: false, error },  {new: true})
    .then(updatedTask => {
      console.log('Emit Error', updatedTask)
      io.emit('updateTask', updatedTask)
      return res.status(200).json(updatedTask)
    })
    .catch(err => {
      console.error(err)
    })
})

module.exports = router