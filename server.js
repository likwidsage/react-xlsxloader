const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const io = require('socket.io')(server)
const cors = require('cors')
const Settings = require('./src/config/Settings')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Api files
const File = require('./api/FileApi')
const Task = require('./api/TaskApi')
const Queue = require('./api/QueueApi')

let connections = []
io.on('connection', socket => {
  connections.push(socket)
  console.log(`== Connected: ${connections.length} connected.`)

  socket.on('disconnect', () => {
    connections.pop(socket)
    console.log('disconnected')
  })
})
app.set('socketio', io)

//Use routes
app.use('/api/file', File)
app.use('/api/tasks', Task)
app.use('/api/queue', Queue)

app.use(express.static('build'))
const path = require('path')
app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, 'build', 'index.html')))

const port = Settings.ApiPort || 5000
server.listen(port, () => {
  console.log(`API on port ${port} in ${app.get('env')} mode`)
})
