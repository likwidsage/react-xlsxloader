import React, { useState, useEffect } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import socketIoClient from 'socket.io-client'

const styles = theme => ({
  root: {
    marginTop: 72,
    marginLeft: 64,
    marginRight: 64,
    flexGrow: 1,
  },
})


const View = (props) => {
  const { classes } = props
  const { id } = props.match.params
  const [task, setTask] = useState({})

  const refresh = () => {
    return new Promise((resolve, reject) => {
      axios.get(`/api/tasks/${id}`).then(res => {
        const task = res.data
        setTask(task)
        resolve(task)
      })
    })
  }


  useEffect(() => {
    refresh().then(task => {
      const socket = socketIoClient()
      socket.on('completed', ioTask => {
        if (ioTask._id === task._id) {
          setTask(ioTask)
        }
      })

      socket.on('running', id => {
        if (task._id === id) {
          const newTask = {
            ...task,
            running: true,
          }
          setTask(newTask)
        }
      })
    })
  }, [])

  useEffect(() => {
    console.log('View - Task', task)
  }, [task])

  return (
    <Grid container className={classes.root}>
      {task._id ?
        <>
          <Grid item xs={1}>
            ID
          </Grid>
          <Grid item xs={11}>
            {task._id}
          </Grid>
          <Grid item xs={1}>
            Processed
          </Grid>
          <Grid item xs={11}>
            {task.processed ? 'Y' : 'N'}
          </Grid>
          <Grid item xs={1}>
            Running
          </Grid>
          <Grid item xs={11}>
            {task.running ? 'Y' : 'N'}
          </Grid>
          <Grid item xs={1}>
            Database Type
          </Grid>
          <Grid item xs={11}>
            {task.dbType}
          </Grid>
          <Grid item xs={1}>
            Write to
          </Grid>
          <Grid item xs={11}>
            {task.tableName}
          </Grid>
          <Grid item xs={1}>
            Error
          </Grid>
          <Grid item xs={11}>
            {task.error ? task.error : 'No Errors'}
          </Grid>
        </>
        : 'Task not found'
      }
    </Grid>
  )
}

View.propTypes = {
  classes: PropTypes.any,
  match: PropTypes.any,
}

export default withStyles(styles)(View)