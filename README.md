Create a .env file with the following values:
```
BASE_URL=[url to webapp]
UPLOAD_DIR=[temp directory to store xlsx files to read]
API_PORT=[port number for api]
ORACLE_USER=[username for oracle database]
ORACLE_PASS=[password to oracle database]
ORACLE_SERVER=[name of database in TNS file]
MONGO_URL=[url to mongo database to store work queue]
```